{"data":[
[
"<a href=https://dle.rae.es/?id=0ignGD5>ademán</a>",
"Quizá del ár. hisp. <i>aḍḍíman</i> o <i>aḍḍamán,</i> y este del ár. clás. <i>ḍamān,</i> literalmente, garantía legal, con cambio de sentido por los gestos exagerados y juramentos con que se ofrecía o se pretendía suplir.<hr>Del lat. <i>ad,</i> a + <i>de,</i> de + <i>manus,</i> mano.<hr>Del árabe إيماءة",
"1. m. Movimiento o actitud del cuerpo o de alguna parte suya con que se manifiesta disposición, intención o sentimiento. <i>Con ademán decidido. Hizo ademán de huir, de acometer.</i><br>2. m. pl. <b>modales.</b>",
"1. m. Movimiento o actitud del cuerpo que manifiesta un estado de ánimo o un sentimiento. <i>La delató un ademán de fatiga.</i> - <b>Gesto.</b><br>2. m. pl. Acciones y modos de comportamiento externos. </i>Sus ademanes evidencian su buena educación.</i> - <b>Modales.</b>",
"1. m. Movimiento o actitud con que se manifiesta un afecto del ánimo.<br><b>En ademán de.</b> <i>loc. adv.</i> En actitud de ir a ejecutar algo.<br>2. m. pl. Modales.",
"1. m. Movimiento corporal que refleja un estado de ánimo. <i>Un ademán de impaciencia.</i> Con determinada actitud. <i>En ademán de ruego.</i><br>2. m. Movimiento corporal que refleja una intención. <i>Hizo un ademán de huir pero el policía lo detuvo.</i>",
"1. m. Acto o gesto que demuestra alguna afección en el ánimo. - <b>Expresión, actitud.</b><br>2. m. Modal (úsase más comúnmente en plural).",
"<b>1.1 Gesto</b><br>Cuando Jim vio a Lucy entrar al restaurante, le hizo un ademán para que se sentara junto a él.<br>Con un ademán, el duque le indicó a su mayordomo que se retirara.<br><b>1.2 Expresión</b><br>Ella no dijo nada, pero su ademán de disgusto expresó claramente lo que sentía.<br><b>1.3 Movimiento</b><br>Llévale la cuenta a la mesa tres. El cliente hizo el ademán de irse del bar.<br><b>2. Modales</b><br>Hay que enseñar a los niños buenos ademanes en la mesa.<hr>Y el buen muchacho se metió el reloj en el bolsillo con soberbio ademán.<br>E hizo ademán de levantarse. Pero Karenin le sujetó por el brazo y le dijo:<br>Ciro Smith hizo un ademán y, al darse cuenta, el capitán añadió:<br>Con rápido ademán se bajó el velo y salió de allí casi a la carrera.<br>Allí hay un tronco – le dijo Iván, con ademán de coger las riendas.<br>Cuando María vio el triste ademán en el rostro de su hijo, se echó a llorar.<br>Hizo el ademán de darle un golpe, y el chico se asustó.<br>El soldado hizo ademán de disparar, pero luego sonrió y bajó la pistola.<br>El bebé parecía dormido, pero cuando hicimos ademán de quitarle el juguete, lo agarró con fuerza.<br>La voz, el ademán y el vestido eran iguales en las dos: Me saludaron con esa unción un poco rancia de las señoras devotas: Las dos sonreían con una sonrisa pueril y meliflua que parecía extenderse en la sombra mística de las mantillas sujetas al peinado con grandes alfilerones de azabache.<br>Pisando el suelo con ademán pulido, barbilucio, gayado de colores el pañuelo, en afeites envuelto, ¿ese tan lucio, tan vestido y compuesto, es algún dije que del país nos vino de Confucio?<br>A veces, esa sensibilidad trascendente se convierte en una constante espera, y cada minuto pasa ante nosotros, con el índice en los labios, en ademán de inminencia.<br>Entró do estaba el convite gentil el recién venido; hizo gracia con el morado sombrero, y atrevido en denodado ademán a doña Blanca se fué; y después de haber pedido su venia, ante ella galán quedó en pie.",
"actitud - amago - aspaviento - brazada - ceremonia - contorsión - expresión - gesto - inclinación - mímica - mohín - momo - mueca - saludo - seña - señas - signo - signos - visaje - manoteo - movimiento - acción - ademanes",
""
],
[
"1",
"2",
"3",
"4",
"5",
"6",
"7",
"8",
"9",
""
]
]
}